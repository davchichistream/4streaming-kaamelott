<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container6xHfX5G\srcApp_KernelProdContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container6xHfX5G/srcApp_KernelProdContainer.php') {
    touch(__DIR__.'/Container6xHfX5G.legacy');

    return;
}

if (!\class_exists(srcApp_KernelProdContainer::class, false)) {
    \class_alias(\Container6xHfX5G\srcApp_KernelProdContainer::class, srcApp_KernelProdContainer::class, false);
}

return new \Container6xHfX5G\srcApp_KernelProdContainer([
    'container.build_hash' => '6xHfX5G',
    'container.build_id' => '5bafed87',
    'container.build_time' => 1636485595,
], __DIR__.\DIRECTORY_SEPARATOR.'Container6xHfX5G');
