<?php

/* saisons/index.html.twig */
class __TwigTemplate_f568b4016f07873084737aee462cd5a57eaac4c5953108dcc3ba373ce7d4a490 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "saisons/index.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Kaamelott Streaming Gratuit en VF et VOSTFR";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Regardez toutes les saisons et tous les épisodes de la série Kaamelott en streaming gratuit VF et VOSTFR !";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container-fluid mt-container-home\">
        <h1 class=\"text-center mb-5\">Kaamelott en streaming gratuit VF et VOSTFR</h1>
        <div class=\"row flex\">
                <div class=\"card-deck deck-home\">
                    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["saisons"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["saison"]) {
            // line 11
            echo "                    <div class=\"card hvr-grow-shadow card-saison saison-last\">
                            <div class=\"ribbon\"><span>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo "</span></div>
                        <img class=\"card-img img-home\" src=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/saisons_directory/" . twig_get_attribute($this->env, $this->source, $context["saison"], "avatar", []))), "html", null, true);
            echo "\" alt=\"Affiche de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Kaamelott\" title=\"Affiche de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Kaamelott\">
                        <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("saison.show", ["slug" => twig_get_attribute($this->env, $this->source, $context["saison"], "slug", []), "id" => twig_get_attribute($this->env, $this->source, $context["saison"], "id", [])]), "html", null, true);
            echo "\" class=\"card-img-overlay hvr-fade\" title=\"Regarder la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Kaamelott en streaming gratuit VF et VOSTFR\">
                            <div class=\"card-title card-titlehome\">
                                <h2>Regarder la ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Kaamelott</h2>
                                <p>";
            // line 17
            echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "description", []), 0, 249), "html", null, true);
            echo "...</p>
                            </div>
                        </a>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['saison'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "saisons/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 22,  86 => 17,  82 => 16,  75 => 14,  67 => 13,  63 => 12,  60 => 11,  56 => 10,  49 => 5,  46 => 4,  40 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "saisons/index.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\4streaming-kaamelott\\templates\\saisons\\index.html.twig");
    }
}
