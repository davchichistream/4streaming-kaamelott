<?php

/* episodes/show.html.twig */
class __TwigTemplate_118c2de79d718e5f92210722ae2920db3f34813b4aebf5efa17456f836b27c05 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "episodes/show.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Kaamelott Streaming - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " / Episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " en VF et VOSTFR";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Regarder l'épisode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de la série Kaamelott en streaming gratuit VF et VOSTFR !";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container mt-container-home\">
        <h1 class=\"mb-3 mt-3\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " / Episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo "</h1>
        <h2 class=\"mb-4 reform-title\">Regarder l'épisode ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Kaamelott en streaming gratuit VF et VOSTFR</h2>
        <div class=\"row mb-4\">
            <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/episodes_directory/" . twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "avatar", []))), "html", null, true);
        echo "\" alt=\"Affiche de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Kaamelott\" class=\"col-2 avatar-img-show\" title=\"Affiche de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Kaamelott\">
            <p class=\"col-md-10\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "description", []), "html", null, true);
        echo " <br><br> <span class=\"font-italic\">Première diffusion: ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "releasedAt", []), "html", null, true);
        echo "</span></p>
            <div class=\"addthis-episode-show addthis_inline_share_toolbox_2cgn\"></div>
        </div>
        <section id=\"tabs\">
            <div class=\"container\">
                <div class=\"row\">
                    ";
        // line 17
        echo $this->extensions['WhiteOctober\BreadcrumbsBundle\Twig\Extension\BreadcrumbsExtension']->renderBreadcrumbs();
        echo "
                </div>

                <div class=\"row mt-2 mb-2\">
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 22
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) == 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 1))) {
            // line 23
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-left btn-show-episode disabled\">< Episode précédent</a>
                        ";
        }
        // line 25
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) > 1))) {
            // line 26
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) - 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 28
        echo "                        ";
        if ((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) > 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 1)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) != 9))) {
            // line 29
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) + 11)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 31
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) == 9) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 1))) {
            // line 32
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) + 11)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 34
        echo "                    </div>
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 36
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) == 9) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 12))) {
            // line 37
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-right btn-show-episode disabled\">Episode suivant ></a>
                        ";
        }
        // line 39
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 12))) {
            // line 40
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) + 1), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) - 11)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 42
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) < 12))) {
            // line 43
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 45
        echo "                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-12 tab-lecteur\">
                        <nav>
                            <div class=\"nav nav-tabs nav-fill\" id=\"nav-tab\" role=\"tablist\">
                                <a class=\"nav-item nav-link active\" id=\"nav-mixdropvf-tab\" title=\"Regarder l'Episode ";
        // line 51
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Kaamelott en streaming VF gratuit\" data-toggle=\"tab\" href=\"#nav-mixdropvf\" role=\"tab\" aria-controls=\"nav-mixdropvf\" aria-selected=\"true\">
                                    Doodstream
                                    <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vf.png"), "html", null, true);
        echo "\" alt=\"Logo version VF pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Kaamelott\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-mixdropvostfr-tab\" data-toggle=\"tab\" href=\"#nav-mixdropvostfr\" role=\"tab\" aria-controls=\"nav-mixdropvostfr\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 55
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Kaamelott en streaming VOSTFR gratuit\">
                                    Doodstream
                                    <img src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vostfr.png"), "html", null, true);
        echo "\" alt=\"Logo version VOSTFR pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Kaamelott\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavf-tab\" data-toggle=\"tab\" href=\"#nav-vidozavf\" role=\"tab\" aria-controls=\"nav-vidozavf\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 59
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Kaamelott en streaming VF gratuit\">
                                    Vidoza
                                    <img src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vf.png"), "html", null, true);
        echo "\" alt=\"Logo version VF pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Kaamelott\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavostfr-tab\" data-toggle=\"tab\" href=\"#nav-vidozavostfr\" role=\"tab\" aria-controls=\"nav-vidozavostfr\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Kaamelott en streaming VOSTFR gratuit\">
                                    Vidoza
                                    <img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vostfr.png"), "html", null, true);
        echo "\" alt=\"Logo version VOSTFR pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Kaamelott\" class=\"logo-version\">
                                </a>
                            </div>
                        </nav>
                        <div class=\"tab-content py-3 px-3 px-sm-0\" id=\"nav-tabContent\">
                            <!--<script src=\"//pubdirecte.com/script/banniere.php?said=128229\"></script>
                            <button class=\"btn btn-secondary btn-show-episode\" id=\"close_pubdirecte\">FERMER PUB</button>-->
                            <div class=\"tab-pane fade show active\" id=\"nav-mixdropvf\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvf-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 74
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVf", [])) > 24)) {
            // line 75
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVf", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 77
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVf", [])) < 24)) {
            // line 78
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    ";
        }
        // line 80
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavf\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavf-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 84
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVf", [])) > 24)) {
            // line 85
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVf", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 87
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVf", [])) < 24)) {
            // line 88
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    ";
        }
        // line 90
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-mixdropvostfr\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvostfr-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 94
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVostfr", [])) > 24)) {
            // line 95
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVostfr", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 97
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVostfr", [])) < 24)) {
            // line 98
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    ";
        }
        // line 100
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavostfr\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavostfr-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 104
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVostfr", [])) > 24)) {
            // line 105
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVostfr", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 107
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVostfr", [])) < 24)) {
            // line 108
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    ";
        }
        // line 110
        echo "                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"row mb-5\">
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 117
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) == 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 1))) {
            // line 118
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-left btn-show-episode disabled\">< Episode précédent</a>
                        ";
        }
        // line 120
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) > 1))) {
            // line 121
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) - 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 123
        echo "                        ";
        if ((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) > 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 1)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) != 9))) {
            // line 124
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) + 11)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 126
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) == 9) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 1))) {
            // line 127
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) + 11)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 129
        echo "                    </div>
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 131
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) == 9) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 12))) {
            // line 132
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-right btn-show-episode disabled\">Episode suivant ></a>
                        ";
        }
        // line 134
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) == 12))) {
            // line 135
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) + 1), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) - 11)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 137
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) < 12))) {
            // line 138
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 140
        echo "                    </div>
                </div>
            </div>
        </section>
    </div>

";
    }

    public function getTemplateName()
    {
        return "episodes/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 140,  379 => 138,  376 => 137,  370 => 135,  367 => 134,  363 => 132,  361 => 131,  357 => 129,  351 => 127,  348 => 126,  342 => 124,  339 => 123,  333 => 121,  330 => 120,  326 => 118,  324 => 117,  315 => 110,  311 => 108,  308 => 107,  302 => 105,  300 => 104,  294 => 100,  290 => 98,  287 => 97,  281 => 95,  279 => 94,  273 => 90,  269 => 88,  266 => 87,  260 => 85,  258 => 84,  252 => 80,  248 => 78,  245 => 77,  239 => 75,  237 => 74,  223 => 65,  216 => 63,  209 => 61,  202 => 59,  195 => 57,  188 => 55,  181 => 53,  174 => 51,  166 => 45,  160 => 43,  157 => 42,  151 => 40,  148 => 39,  144 => 37,  142 => 36,  138 => 34,  132 => 32,  129 => 31,  123 => 29,  120 => 28,  114 => 26,  111 => 25,  107 => 23,  105 => 22,  97 => 17,  86 => 11,  78 => 10,  71 => 8,  63 => 7,  59 => 5,  56 => 4,  46 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "episodes/show.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\4streaming-kaamelott\\templates\\episodes\\show.html.twig");
    }
}
