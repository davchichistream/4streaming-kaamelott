<?php

/* saisons/show.html.twig */
class __TwigTemplate_f436d27417c1f9136730c6ddb3cb76d7365475b89929c6b093720f0d5a5750e3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "saisons/show.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "saisons/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "saisons/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Dexter Streaming - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 2, $this->source); })()), "name", []), "html", null, true);
        echo " en VF et VOSTFR";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        echo "Regarder tous les épisodes de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 3, $this->source); })()), "name", []), "html", null, true);
        echo " de la série Dexter en streaming gratuit VF et VOSTFR!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <div class=\"container mt-container-home\">
        <h1 class=\"mb-3 mt-3\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 7, $this->source); })()), "name", []), "html", null, true);
        echo " de Dexter</h1>
        <h2 class=\"mb-4 reform-title\">Regarder les épisodes de la ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 8, $this->source); })()), "name", []), "html", null, true);
        echo " de la série Dexter en streaming gratuit VF et VOSTFR</h2>
        <div class=\"row mb-4\">
            <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/saisons_directory/" . twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 10, $this->source); })()), "avatar", []))), "html", null, true);
        echo "\" alt=\"Affiche de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 10, $this->source); })()), "name", []), "html", null, true);
        echo " de Dexter\" title=\"Affiche de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 10, $this->source); })()), "name", []), "html", null, true);
        echo " de Dexter\" class=\"col-2 avatar-img-show\">
            <p class=\"col-md-10\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 11, $this->source); })()), "description", []), "html", null, true);
        echo "</p>
            <div class=\"addthis-episode-show addthis_inline_share_toolbox_hrjo\"></div>
        </div>

        <div class=\"container\">
            <div class=\"card-deck deck-show\">
                ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["episodes"]) || array_key_exists("episodes", $context) ? $context["episodes"] : (function () { throw new Twig_Error_Runtime('Variable "episodes" does not exist.', 17, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["episode"]) {
            // line 18
            echo "                    <div class=\"card card-show card-last-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "last", []), "html", null, true);
            echo "\">
                        <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 19, $this->source); })()), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 19, $this->source); })()), "id", []), "slug" => twig_get_attribute($this->env, $this->source, $context["episode"], "slug", []), "id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", []), "number" => twig_get_attribute($this->env, $this->source, $context["episode"], "number", [])]), "html", null, true);
            echo "\" class=\"img-show\" title=\"Regarder l'Episode ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "number", []), "html", null, true);
            echo " de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 19, $this->source); })()), "name", []), "html", null, true);
            echo " en streaming gratuit VF et VOSTFR\">
                            <img class=\"card-img-top\" src=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/episodes_directory/" . twig_get_attribute($this->env, $this->source, $context["episode"], "avatar", []))), "html", null, true);
            echo "\" alt=\"Affiche de ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "title", []), "html", null, true);
            echo " de Dexter\">
                        </a>
                        <div class=\"card-body body-show\">
                            <h3 class=\"card-title\">Episode ";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "number", []), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "title", []), "html", null, true);
            echo " </h3>
                            <p class=\"card-text\">";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "description", []), "html", null, true);
            echo "</p>
                        </div>
                        <a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 26, $this->source); })()), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 26, $this->source); })()), "id", []), "slug" => twig_get_attribute($this->env, $this->source, $context["episode"], "slug", []), "id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", []), "number" => twig_get_attribute($this->env, $this->source, $context["episode"], "number", [])]), "html", null, true);
            echo "\" class=\"btn btn-secondary btn-lg btn-show\" role=\"button\" title=\"Regarder l'Episode ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "number", []), "html", null, true);
            echo " de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["saison"]) || array_key_exists("saison", $context) ? $context["saison"] : (function () { throw new Twig_Error_Runtime('Variable "saison" does not exist.', 26, $this->source); })()), "name", []), "html", null, true);
            echo " en streaming gratuit VF et VOSTFR\">Regarder</a>
                    </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['episode'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "            </div>
        </div>
    </div>




";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "saisons/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 29,  174 => 26,  169 => 24,  163 => 23,  155 => 20,  147 => 19,  142 => 18,  125 => 17,  116 => 11,  108 => 10,  103 => 8,  99 => 7,  95 => 5,  86 => 4,  66 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %}Dexter Streaming - {{ saison.name }} en VF et VOSTFR{% endblock %}
{% block meta %}Regarder tous les épisodes de la {{ saison.name }} de la série Dexter en streaming gratuit VF et VOSTFR!{% endblock %}
{% block body %}

    <div class=\"container mt-container-home\">
        <h1 class=\"mb-3 mt-3\">{{ saison.name }} de Dexter</h1>
        <h2 class=\"mb-4 reform-title\">Regarder les épisodes de la {{ saison.name }} de la série Dexter en streaming gratuit VF et VOSTFR</h2>
        <div class=\"row mb-4\">
            <img src=\"{{ asset('uploads/saisons_directory/' ~ saison.avatar) }}\" alt=\"Affiche de la {{ saison.name }} de Dexter\" title=\"Affiche de la {{ saison.name }} de Dexter\" class=\"col-2 avatar-img-show\">
            <p class=\"col-md-10\">{{ saison.description }}</p>
            <div class=\"addthis-episode-show addthis_inline_share_toolbox_hrjo\"></div>
        </div>

        <div class=\"container\">
            <div class=\"card-deck deck-show\">
                {% for episode in episodes %}
                    <div class=\"card card-show card-last-{{ loop.last }}\">
                        <a href=\"{{ path('episode.show', {slug_s: saison.slug, id_s: saison.id, slug: episode.slug, id: episode.id, number: episode.number}) }}\" class=\"img-show\" title=\"Regarder l'Episode {{ episode.number }} de la {{ saison.name }} en streaming gratuit VF et VOSTFR\">
                            <img class=\"card-img-top\" src=\"{{ asset('uploads/episodes_directory/' ~ episode.avatar) }}\" alt=\"Affiche de {{ episode.title }} de Dexter\">
                        </a>
                        <div class=\"card-body body-show\">
                            <h3 class=\"card-title\">Episode {{ episode.number }} - {{ episode.title }} </h3>
                            <p class=\"card-text\">{{ episode.description }}</p>
                        </div>
                        <a href=\"{{ path('episode.show', {slug_s: saison.slug, id_s: saison.id, slug: episode.slug, id: episode.id, number: episode.number}) }}\" class=\"btn btn-secondary btn-lg btn-show\" role=\"button\" title=\"Regarder l'Episode {{ episode.number }} de la {{ saison.name }} en streaming gratuit VF et VOSTFR\">Regarder</a>
                    </div>
                {% endfor %}
            </div>
        </div>
    </div>




{% endblock %}", "saisons/show.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\4streaming-dexter\\templates\\saisons\\show.html.twig");
    }
}
