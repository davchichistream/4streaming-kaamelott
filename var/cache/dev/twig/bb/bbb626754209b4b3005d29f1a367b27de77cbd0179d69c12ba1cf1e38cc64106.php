<?php

/* saisons/series.html.twig */
class __TwigTemplate_db40cf7661f92ae6e3b646cabb7b9c759b12135abb146bdb7bd431492d7b1650 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "saisons/series.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "saisons/series.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "saisons/series.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Regarder d'autres séries sur Dexter Streaming Gratuit en VF et VOSTFR";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        echo "Regardez d'autres séries sur Dexter streaming gratuit en VF et VOSTFR !";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <div class=\"container-fluid mt-container-home\">
        <h1 class=\"text-center mb-5\">Dexter streaming gratuit VF et VOSTFR - Séries</h1>
        <div class=\"row flex\">
            <div class=\"card-deck deck-home\">
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/dexter.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de dexter\" title=\"Affiche de dexter\">
                    <a href=\"https://dexter-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Dexter en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/got.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de game of thrones\" title=\"Affiche de game of thrones\">
                    <a href=\"https://got-streaming-gratuit.com\" class=\"card-img-overlay hvr-fade\" title=\"série Game of Thrones en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/harrypotter.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Harry Potter\" title=\"Affiche de Harry Potter\">
                    <a href=\"https://harry-potter-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"Harry Potter en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/onepiece.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de One Piece\" title=\"Affiche de One Piece\">
                    <a href=\"https://voir-one-piece-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série One Piece en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/demon.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Demon Slayer\" title=\"Affiche de Demon Slayer\">
                    <a href=\"https://demon-slayer-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Demon Slayer en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/shippuden.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Naruto Shippuden\" title=\"Affiche de Naruto Shippuden\">
                    <a href=\"https://naruto-shippuden-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Naruto Shippuden en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/academia.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de My Hero Academia\" title=\"Affiche de My Hero Academia\">
                    <a href=\"https://myheroacademia-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série My Hero Academia en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/titans.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Attaque des Titans\" title=\"Affiche de Attaque des Titans\">
                    <a href=\"https://attaquedestitans-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Attaque des Titans en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/bleach.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Bleach\" title=\"Affiche de Bleach\">
                    <a href=\"https://bleach-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Bleach en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/vikings.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Vikings\" title=\"Affiche de the Vikings\">
                    <a href=\"https://vikings-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Vikings en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/100.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de the 100\" title=\"Affiche de the 100\">
                    <a href=\"https://regarder-the-100-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série the 100 en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/stranger.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de sranger things\" title=\"Affiche de stranger things\">
                    <a href=\"https://strangerthings-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Stranger Things en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/witcher.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de the witcher\" title=\"Affiche de the witcher\">
                    <a href=\"https://regarder-the-witcher-streaming-gratuit.com\" class=\"card-img-overlay hvr-fade\" title=\"série The Witcher en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/malcolm.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de malcolm\" title=\"Affiche de malcolm\">
                    <a href=\"https://regarder-malcolm-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Malcolm en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/walking.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de The walking dead\" title=\"Affiche de The walking dead\">
                    <a href=\"https://the-walking-dead-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série The walking dead en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/anatomy.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Grey's Anatomy\" title=\"Affiche de Grey's Anatomy\">
                    <a href=\"https://greys-anatomy-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Grey's Anatomy en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/peaky.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Peaky Blinders\" title=\"Affiche de Peaky Blinders\">
                    <a href=\"https://regarder-peaky-blinders-streaming.xyz\" class=\"card-img-overlay hvr-fade\" title=\"série Peaky Blinders en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/american.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de American Horror Story\" title=\"Affiche de American Horror Story\">
                    <a href=\"https://americanhorrorstory-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série American Horror Story en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/prison.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Prison Break\" title=\"Affiche de Prison Break\">
                    <a href=\"https://regarder-prison-break-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Prison Break en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/watchmen.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Watchmen\" title=\"Affiche de Watchmen\">
                    <a href=\"https://regarder-watchmen-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Watchmen en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/materials.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de His Dark Materials\" title=\"Affiche de His Dark Materials\">
                    <a href=\"https://regarder-his-dark-materials-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série His Dark Materials en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/bureau.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Bureau des légendes\" title=\"Affiche de Bureau des légendes\">
                    <a href=\"https://regarder-le-bureau-des-legendes-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Bureau des légendes en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/famille.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Ma Famille D'abord\" title=\"Affiche de Ma Famille D'abord\">
                    <a href=\"https://ma-famille-dabord-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Ma Famille D'abord en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/mirror.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Black Mirror\" title=\"Affiche de Black Mirror\">
                    <a href=\"https://black-mirror-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Black Mirror en streaming\">
                    </a>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "saisons/series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 126,  275 => 121,  267 => 116,  259 => 111,  251 => 106,  243 => 101,  235 => 96,  227 => 91,  219 => 86,  211 => 81,  203 => 76,  195 => 71,  187 => 66,  179 => 61,  171 => 56,  163 => 51,  155 => 46,  147 => 41,  139 => 36,  131 => 31,  123 => 26,  115 => 21,  107 => 16,  99 => 11,  91 => 5,  82 => 4,  64 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %}Regarder d'autres séries sur Dexter Streaming Gratuit en VF et VOSTFR{% endblock %}
{% block meta %}Regardez d'autres séries sur Dexter streaming gratuit en VF et VOSTFR !{% endblock %}
{% block body %}

    <div class=\"container-fluid mt-container-home\">
        <h1 class=\"text-center mb-5\">Dexter streaming gratuit VF et VOSTFR - Séries</h1>
        <div class=\"row flex\">
            <div class=\"card-deck deck-home\">
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/dexter.jpg') }}\" alt=\"Affiche de dexter\" title=\"Affiche de dexter\">
                    <a href=\"https://dexter-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Dexter en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/got.jpg') }}\" alt=\"Affiche de game of thrones\" title=\"Affiche de game of thrones\">
                    <a href=\"https://got-streaming-gratuit.com\" class=\"card-img-overlay hvr-fade\" title=\"série Game of Thrones en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/harrypotter.jpg') }}\" alt=\"Affiche de Harry Potter\" title=\"Affiche de Harry Potter\">
                    <a href=\"https://harry-potter-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"Harry Potter en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/onepiece.jpg') }}\" alt=\"Affiche de One Piece\" title=\"Affiche de One Piece\">
                    <a href=\"https://voir-one-piece-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série One Piece en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/demon.jpg') }}\" alt=\"Affiche de Demon Slayer\" title=\"Affiche de Demon Slayer\">
                    <a href=\"https://demon-slayer-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Demon Slayer en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/shippuden.jpg') }}\" alt=\"Affiche de Naruto Shippuden\" title=\"Affiche de Naruto Shippuden\">
                    <a href=\"https://naruto-shippuden-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Naruto Shippuden en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/academia.jpg') }}\" alt=\"Affiche de My Hero Academia\" title=\"Affiche de My Hero Academia\">
                    <a href=\"https://myheroacademia-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série My Hero Academia en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/titans.jpg') }}\" alt=\"Affiche de Attaque des Titans\" title=\"Affiche de Attaque des Titans\">
                    <a href=\"https://attaquedestitans-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Attaque des Titans en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/bleach.jpg') }}\" alt=\"Affiche de Bleach\" title=\"Affiche de Bleach\">
                    <a href=\"https://bleach-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Bleach en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/vikings.jpg') }}\" alt=\"Affiche de Vikings\" title=\"Affiche de the Vikings\">
                    <a href=\"https://vikings-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Vikings en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/100.jpg') }}\" alt=\"Affiche de the 100\" title=\"Affiche de the 100\">
                    <a href=\"https://regarder-the-100-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série the 100 en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/stranger.jpg') }}\" alt=\"Affiche de sranger things\" title=\"Affiche de stranger things\">
                    <a href=\"https://strangerthings-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Stranger Things en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/witcher.jpg') }}\" alt=\"Affiche de the witcher\" title=\"Affiche de the witcher\">
                    <a href=\"https://regarder-the-witcher-streaming-gratuit.com\" class=\"card-img-overlay hvr-fade\" title=\"série The Witcher en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/malcolm.jpg') }}\" alt=\"Affiche de malcolm\" title=\"Affiche de malcolm\">
                    <a href=\"https://regarder-malcolm-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Malcolm en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/walking.jpg') }}\" alt=\"Affiche de The walking dead\" title=\"Affiche de The walking dead\">
                    <a href=\"https://the-walking-dead-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série The walking dead en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/anatomy.jpg') }}\" alt=\"Affiche de Grey's Anatomy\" title=\"Affiche de Grey's Anatomy\">
                    <a href=\"https://greys-anatomy-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Grey's Anatomy en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/peaky.jpg') }}\" alt=\"Affiche de Peaky Blinders\" title=\"Affiche de Peaky Blinders\">
                    <a href=\"https://regarder-peaky-blinders-streaming.xyz\" class=\"card-img-overlay hvr-fade\" title=\"série Peaky Blinders en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/american.jpg') }}\" alt=\"Affiche de American Horror Story\" title=\"Affiche de American Horror Story\">
                    <a href=\"https://americanhorrorstory-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série American Horror Story en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/prison.jpg') }}\" alt=\"Affiche de Prison Break\" title=\"Affiche de Prison Break\">
                    <a href=\"https://regarder-prison-break-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Prison Break en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/watchmen.jpg') }}\" alt=\"Affiche de Watchmen\" title=\"Affiche de Watchmen\">
                    <a href=\"https://regarder-watchmen-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Watchmen en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/materials.jpg') }}\" alt=\"Affiche de His Dark Materials\" title=\"Affiche de His Dark Materials\">
                    <a href=\"https://regarder-his-dark-materials-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série His Dark Materials en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/bureau.jpg') }}\" alt=\"Affiche de Bureau des légendes\" title=\"Affiche de Bureau des légendes\">
                    <a href=\"https://regarder-le-bureau-des-legendes-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Bureau des légendes en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/famille.jpg') }}\" alt=\"Affiche de Ma Famille D'abord\" title=\"Affiche de Ma Famille D'abord\">
                    <a href=\"https://ma-famille-dabord-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Ma Famille D'abord en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"{{ asset('uploads/series_directory/mirror.jpg') }}\" alt=\"Affiche de Black Mirror\" title=\"Affiche de Black Mirror\">
                    <a href=\"https://black-mirror-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Black Mirror en streaming\">
                    </a>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", "saisons/series.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\4streaming-dexter\\templates\\saisons\\series.html.twig");
    }
}
