<?php

/* episodes/show.html.twig */
class __TwigTemplate_c25f24a330351f35dd11c7d340ade43f0c592aa715efd6a1a4d1a661d68a380e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "episodes/show.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "episodes/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "episodes/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Dexter Streaming - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 2, $this->source); })()), "saison", []), "html", null, true);
        echo " / Episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 2, $this->source); })()), "number", []), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 2, $this->source); })()), "title", []), "html", null, true);
        echo " en VF et VOSTFR";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        echo "Regarder l'épisode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 3, $this->source); })()), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 3, $this->source); })()), "saison", []), "html", null, true);
        echo " de la série Dexter en streaming gratuit VF et VOSTFR !";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <div class=\"container mt-container-home\">
        <h1 class=\"mb-3 mt-3\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 7, $this->source); })()), "saison", []), "html", null, true);
        echo " / Episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 7, $this->source); })()), "number", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 7, $this->source); })()), "title", []), "html", null, true);
        echo "</h1>
        <h2 class=\"mb-4 reform-title\">Regarder l'épisode ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 8, $this->source); })()), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 8, $this->source); })()), "saison", []), "html", null, true);
        echo " de Dexter en streaming gratuit VF et VOSTFR</h2>
        <div class=\"row mb-4\">
            <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/episodes_directory/" . twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 10, $this->source); })()), "avatar", []))), "html", null, true);
        echo "\" alt=\"Affiche de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 10, $this->source); })()), "title", []), "html", null, true);
        echo " de Dexter\" class=\"col-2 avatar-img-show\" title=\"Affiche de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 10, $this->source); })()), "title", []), "html", null, true);
        echo " de Dexter\">
            <p class=\"col-md-10\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 11, $this->source); })()), "description", []), "html", null, true);
        echo " <br><br> <span class=\"font-italic\">Première diffusion: ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 11, $this->source); })()), "releasedAt", []), "html", null, true);
        echo "</span></p>
            <!-- <div class=\"addthis-episode-show addthis_inline_share_toolbox_hrjo\"></div> -->
        </div>
        <section id=\"tabs\">
            <div class=\"container\">
                <div class=\"row\">
                    ";
        // line 17
        echo $this->extensions['WhiteOctober\BreadcrumbsBundle\Twig\Extension\BreadcrumbsExtension']->renderBreadcrumbs();
        echo "
                </div>

                <div class=\"row mt-2 mb-2\">
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 22
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 22, $this->source); })()), "saison", []), "id", []) == 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 22, $this->source); })()), "number", []) == 1))) {
            // line 23
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-left btn-show-episode disabled\">< Episode précédent</a>
                        ";
        }
        // line 25
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 25, $this->source); })()), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 25, $this->source); })()), "number", []) > 1))) {
            // line 26
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 26, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 26, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 26, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 26, $this->source); })()), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 26, $this->source); })()), "number", []) - 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 28
        echo "                        ";
        if ((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 28, $this->source); })()), "saison", []), "id", []) > 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 28, $this->source); })()), "number", []) == 1)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 28, $this->source); })()), "saison", []), "id", []) != 8))) {
            // line 29
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 29, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 29, $this->source); })()), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 29, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 29, $this->source); })()), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 29, $this->source); })()), "number", []) + 9)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 31
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 31, $this->source); })()), "saison", []), "id", []) == 8) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 31, $this->source); })()), "number", []) == 1))) {
            // line 32
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 32, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 32, $this->source); })()), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 32, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 32, $this->source); })()), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 32, $this->source); })()), "number", []) + 6)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 34
        echo "                    </div>
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 36
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 36, $this->source); })()), "saison", []), "id", []) == 8) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 36, $this->source); })()), "number", []) == 6))) {
            // line 37
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-right btn-show-episode disabled\">Episode suivant ></a>
                        ";
        }
        // line 39
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 39, $this->source); })()), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 39, $this->source); })()), "number", []) == 10))) {
            // line 40
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 40, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 40, $this->source); })()), "saison", []), "id", []) + 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 40, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 40, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 40, $this->source); })()), "number", []) - 9)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 42
        echo "                        ";
        if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 42, $this->source); })()), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 42, $this->source); })()), "number", []) < 10)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 42, $this->source); })()), "saison", []), "id", []) != 7)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 42, $this->source); })()), "saison", []), "id", []) != 8))) {
            // line 43
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 43, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 43, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 43, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 43, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 43, $this->source); })()), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 45
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 45, $this->source); })()), "saison", []), "id", []) == 7) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 45, $this->source); })()), "number", []) == 7))) {
            // line 46
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 46, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 46, $this->source); })()), "saison", []), "id", []) + 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 46, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 46, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 46, $this->source); })()), "number", []) - 6)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 48
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 48, $this->source); })()), "saison", []), "id", []) == 7) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 48, $this->source); })()), "number", []) < 7))) {
            // line 49
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 49, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 49, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 49, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 49, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 49, $this->source); })()), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 51
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 51, $this->source); })()), "saison", []), "id", []) == 8) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 51, $this->source); })()), "number", []) < 6))) {
            // line 52
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 52, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 52, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 52, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 52, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 52, $this->source); })()), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 54
        echo "                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-12 tab-lecteur\">
                        <nav>
                            <div class=\"nav nav-tabs nav-fill\" id=\"nav-tab\" role=\"tablist\">
                                <a class=\"nav-item nav-link active\" id=\"nav-mixdropvf-tab\" title=\"Regarder l'Episode ";
        // line 60
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 60, $this->source); })()), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 60, $this->source); })()), "saison", []), "html", null, true);
        echo " de Dexter en streaming VF gratuit\" data-toggle=\"tab\" href=\"#nav-mixdropvf\" role=\"tab\" aria-controls=\"nav-mixdropvf\" aria-selected=\"true\">
                                    Doodstream
                                    <img src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vf.png"), "html", null, true);
        echo "\" alt=\"Logo version VF pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 62, $this->source); })()), "title", []), "html", null, true);
        echo " de Dexter\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-mixdropvostfr-tab\" data-toggle=\"tab\" href=\"#nav-mixdropvostfr\" role=\"tab\" aria-controls=\"nav-mixdropvostfr\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 64
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 64, $this->source); })()), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 64, $this->source); })()), "saison", []), "html", null, true);
        echo " de Dexter en streaming VOSTFR gratuit\">
                                    Doodstream
                                    <img src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vostfr.png"), "html", null, true);
        echo "\" alt=\"Logo version VOSTFR pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 66, $this->source); })()), "title", []), "html", null, true);
        echo " de Dexter\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavf-tab\" data-toggle=\"tab\" href=\"#nav-vidozavf\" role=\"tab\" aria-controls=\"nav-vidozavf\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 68
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 68, $this->source); })()), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 68, $this->source); })()), "saison", []), "html", null, true);
        echo " de Dexter en streaming VF gratuit\">
                                    Vidoza
                                    <img src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vf.png"), "html", null, true);
        echo "\" alt=\"Logo version VF pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 70, $this->source); })()), "title", []), "html", null, true);
        echo " de Dexter\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavostfr-tab\" data-toggle=\"tab\" href=\"#nav-vidozavostfr\" role=\"tab\" aria-controls=\"nav-vidozavostfr\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 72
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 72, $this->source); })()), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 72, $this->source); })()), "saison", []), "html", null, true);
        echo " de Dexter en streaming VOSTFR gratuit\">
                                    Vidoza
                                    <img src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vostfr.png"), "html", null, true);
        echo "\" alt=\"Logo version VOSTFR pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 74, $this->source); })()), "title", []), "html", null, true);
        echo " de Dexter\" class=\"logo-version\">
                                </a>
                            </div>
                        </nav>
                        <div class=\"tab-content py-3 px-3 px-sm-0\" id=\"nav-tabContent\">
                            <!--<script src=\"//pubdirecte.com/script/banniere.php?said=128229\"></script>
                            <button class=\"btn btn-secondary btn-show-episode\" id=\"close_pubdirecte\">FERMER PUB</button>-->
                            <div class=\"tab-pane fade show active\" id=\"nav-mixdropvf\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvf-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 83
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 83, $this->source); })()), "OpenloadLinkVf", [])) > 24)) {
            // line 84
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 84, $this->source); })()), "OpenloadLinkVf", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 86
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 86, $this->source); })()), "OpenloadLinkVf", [])) < 24)) {
            // line 87
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    ";
        }
        // line 89
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavf\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavf-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 93
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 93, $this->source); })()), "StreamangoLinkVf", [])) > 24)) {
            // line 94
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 94, $this->source); })()), "StreamangoLinkVf", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 96
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 96, $this->source); })()), "StreamangoLinkVf", [])) < 24)) {
            // line 97
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    ";
        }
        // line 99
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-mixdropvostfr\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvostfr-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 103
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 103, $this->source); })()), "OpenloadLinkVostfr", [])) > 24)) {
            // line 104
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 104, $this->source); })()), "OpenloadLinkVostfr", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 106
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 106, $this->source); })()), "OpenloadLinkVostfr", [])) < 24)) {
            // line 107
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    ";
        }
        // line 109
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavostfr\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavostfr-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 113
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 113, $this->source); })()), "StreamangoLinkVostfr", [])) > 24)) {
            // line 114
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 114, $this->source); })()), "StreamangoLinkVostfr", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 116
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 116, $this->source); })()), "StreamangoLinkVostfr", [])) < 24)) {
            // line 117
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    ";
        }
        // line 119
        echo "                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"row mb-5\">
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 126
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 126, $this->source); })()), "saison", []), "id", []) == 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 126, $this->source); })()), "number", []) == 1))) {
            // line 127
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-left btn-show-episode disabled\">< Episode précédent</a>
                        ";
        }
        // line 129
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 129, $this->source); })()), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 129, $this->source); })()), "number", []) > 1))) {
            // line 130
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 130, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 130, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 130, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 130, $this->source); })()), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 130, $this->source); })()), "number", []) - 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 132
        echo "                        ";
        if ((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 132, $this->source); })()), "saison", []), "id", []) > 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 132, $this->source); })()), "number", []) == 1)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 132, $this->source); })()), "saison", []), "id", []) != 8))) {
            // line 133
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 133, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 133, $this->source); })()), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 133, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 133, $this->source); })()), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 133, $this->source); })()), "number", []) + 9)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 135
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 135, $this->source); })()), "saison", []), "id", []) == 8) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 135, $this->source); })()), "number", []) == 1))) {
            // line 136
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 136, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 136, $this->source); })()), "saison", []), "id", []) - 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 136, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 136, $this->source); })()), "id", []) - 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 136, $this->source); })()), "number", []) + 6)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        ";
        }
        // line 138
        echo "                    </div>
                    <div class=\"col-6 div-btn-show-episode\">
                        ";
        // line 140
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 140, $this->source); })()), "saison", []), "id", []) == 8) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 140, $this->source); })()), "number", []) == 6))) {
            // line 141
            echo "                            <a href=\"#\" class=\"btn btn-secondary float-right btn-show-episode disabled\">Episode suivant ></a>
                        ";
        }
        // line 143
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 143, $this->source); })()), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 143, $this->source); })()), "number", []) == 10))) {
            // line 144
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 144, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 144, $this->source); })()), "saison", []), "id", []) + 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 144, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 144, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 144, $this->source); })()), "number", []) - 9)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 146
        echo "                        ";
        if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 146, $this->source); })()), "saison", []), "id", []) >= 1) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 146, $this->source); })()), "number", []) < 10)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 146, $this->source); })()), "saison", []), "id", []) != 7)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 146, $this->source); })()), "saison", []), "id", []) != 8))) {
            // line 147
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 147, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 147, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 147, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 147, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 147, $this->source); })()), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 149
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 149, $this->source); })()), "saison", []), "id", []) == 7) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 149, $this->source); })()), "number", []) == 7))) {
            // line 150
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 150, $this->source); })()), "saison", []), "slug", []), "id_s" => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 150, $this->source); })()), "saison", []), "id", []) + 1), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 150, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 150, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 150, $this->source); })()), "number", []) - 6)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 152
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 152, $this->source); })()), "saison", []), "id", []) == 7) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 152, $this->source); })()), "number", []) < 7))) {
            // line 153
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 153, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 153, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 153, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 153, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 153, $this->source); })()), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 155
        echo "                        ";
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 155, $this->source); })()), "saison", []), "id", []) == 8) && (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 155, $this->source); })()), "number", []) < 6))) {
            // line 156
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 156, $this->source); })()), "saison", []), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 156, $this->source); })()), "saison", []), "id", []), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 156, $this->source); })()), "slug", []), "id" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 156, $this->source); })()), "id", []) + 1), "number" => (twig_get_attribute($this->env, $this->source, (isset($context["episode"]) || array_key_exists("episode", $context) ? $context["episode"] : (function () { throw new Twig_Error_Runtime('Variable "episode" does not exist.', 156, $this->source); })()), "number", []) + 1)]), "html", null, true);
            echo "\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        ";
        }
        // line 158
        echo "                    </div>
                </div>
            </div>
        </section>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "episodes/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  481 => 158,  475 => 156,  472 => 155,  466 => 153,  463 => 152,  457 => 150,  454 => 149,  448 => 147,  445 => 146,  439 => 144,  436 => 143,  432 => 141,  430 => 140,  426 => 138,  420 => 136,  417 => 135,  411 => 133,  408 => 132,  402 => 130,  399 => 129,  395 => 127,  393 => 126,  384 => 119,  380 => 117,  377 => 116,  371 => 114,  369 => 113,  363 => 109,  359 => 107,  356 => 106,  350 => 104,  348 => 103,  342 => 99,  338 => 97,  335 => 96,  329 => 94,  327 => 93,  321 => 89,  317 => 87,  314 => 86,  308 => 84,  306 => 83,  292 => 74,  285 => 72,  278 => 70,  271 => 68,  264 => 66,  257 => 64,  250 => 62,  243 => 60,  235 => 54,  229 => 52,  226 => 51,  220 => 49,  217 => 48,  211 => 46,  208 => 45,  202 => 43,  199 => 42,  193 => 40,  190 => 39,  186 => 37,  184 => 36,  180 => 34,  174 => 32,  171 => 31,  165 => 29,  162 => 28,  156 => 26,  153 => 25,  149 => 23,  147 => 22,  139 => 17,  128 => 11,  120 => 10,  113 => 8,  105 => 7,  101 => 5,  92 => 4,  70 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %}Dexter Streaming - {{ episode.saison }} / Episode {{ episode.number }} - {{ episode.title }} en VF et VOSTFR{% endblock %}
{% block meta %}Regarder l'épisode {{ episode.number }} de la {{ episode.saison }} de la série Dexter en streaming gratuit VF et VOSTFR !{% endblock %}
{% block body %}

    <div class=\"container mt-container-home\">
        <h1 class=\"mb-3 mt-3\">{{ episode.saison }} / Episode {{ episode.number }} : {{ episode.title }}</h1>
        <h2 class=\"mb-4 reform-title\">Regarder l'épisode {{ episode.number }} de la {{ episode.saison }} de Dexter en streaming gratuit VF et VOSTFR</h2>
        <div class=\"row mb-4\">
            <img src=\"{{ asset('uploads/episodes_directory/' ~ episode.avatar) }}\" alt=\"Affiche de {{ episode.title }} de Dexter\" class=\"col-2 avatar-img-show\" title=\"Affiche de {{ episode.title }} de Dexter\">
            <p class=\"col-md-10\">{{ episode.description }} <br><br> <span class=\"font-italic\">Première diffusion: {{ episode.releasedAt }}</span></p>
            <!-- <div class=\"addthis-episode-show addthis_inline_share_toolbox_hrjo\"></div> -->
        </div>
        <section id=\"tabs\">
            <div class=\"container\">
                <div class=\"row\">
                    {{ wo_render_breadcrumbs() }}
                </div>

                <div class=\"row mt-2 mb-2\">
                    <div class=\"col-6 div-btn-show-episode\">
                        {% if episode.saison.id == 1 and episode.number == 1 %}
                            <a href=\"#\" class=\"btn btn-secondary float-left btn-show-episode disabled\">< Episode précédent</a>
                        {% endif %}
                        {% if episode.saison.id >= 1 and episode.number > 1 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id - 1, number: episode.number - 1}) }}\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        {% endif %}
                        {% if episode.saison.id > 1 and episode.number == 1 and episode.saison.id != 8 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id - 1, slug: episode.slug, id: episode.id - 1, number: episode.number + 9}) }}\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        {% endif %}
                        {% if episode.saison.id == 8 and episode.number == 1 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id - 1, slug: episode.slug, id: episode.id - 1, number: episode.number + 6}) }}\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        {% endif %}
                    </div>
                    <div class=\"col-6 div-btn-show-episode\">
                        {% if episode.saison.id == 8 and episode.number == 6 %}
                            <a href=\"#\" class=\"btn btn-secondary float-right btn-show-episode disabled\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id >= 1 and episode.number == 10 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id + 1, slug: episode.slug, id: episode.id + 1, number: episode.number - 9}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id >= 1 and episode.number < 10 and episode.saison.id != 7 and episode.saison.id != 8 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id + 1, number: episode.number + 1}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id == 7 and episode.number == 7 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id + 1, slug: episode.slug, id: episode.id + 1, number: episode.number - 6}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id == 7 and episode.number < 7 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id + 1, number: episode.number + 1}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id == 8 and episode.number < 6 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id + 1, number: episode.number + 1}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-12 tab-lecteur\">
                        <nav>
                            <div class=\"nav nav-tabs nav-fill\" id=\"nav-tab\" role=\"tablist\">
                                <a class=\"nav-item nav-link active\" id=\"nav-mixdropvf-tab\" title=\"Regarder l'Episode {{ episode.number }} de la {{ episode.saison }} de Dexter en streaming VF gratuit\" data-toggle=\"tab\" href=\"#nav-mixdropvf\" role=\"tab\" aria-controls=\"nav-mixdropvf\" aria-selected=\"true\">
                                    Doodstream
                                    <img src=\"{{ asset('uploads/logos_directory/vf.png') }}\" alt=\"Logo version VF pour episode {{ episode.title }} de Dexter\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-mixdropvostfr-tab\" data-toggle=\"tab\" href=\"#nav-mixdropvostfr\" role=\"tab\" aria-controls=\"nav-mixdropvostfr\" aria-selected=\"false\" title=\"Regarder l'Episode {{ episode.number }} de la {{ episode.saison }} de Dexter en streaming VOSTFR gratuit\">
                                    Doodstream
                                    <img src=\"{{ asset('uploads/logos_directory/vostfr.png') }}\" alt=\"Logo version VOSTFR pour episode {{ episode.title }} de Dexter\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavf-tab\" data-toggle=\"tab\" href=\"#nav-vidozavf\" role=\"tab\" aria-controls=\"nav-vidozavf\" aria-selected=\"false\" title=\"Regarder l'Episode {{ episode.number }} de la {{ episode.saison }} de Dexter en streaming VF gratuit\">
                                    Vidoza
                                    <img src=\"{{ asset('uploads/logos_directory/vf.png') }}\" alt=\"Logo version VF pour episode {{ episode.title }} de Dexter\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavostfr-tab\" data-toggle=\"tab\" href=\"#nav-vidozavostfr\" role=\"tab\" aria-controls=\"nav-vidozavostfr\" aria-selected=\"false\" title=\"Regarder l'Episode {{ episode.number }} de la {{ episode.saison }} de Dexter en streaming VOSTFR gratuit\">
                                    Vidoza
                                    <img src=\"{{ asset('uploads/logos_directory/vostfr.png') }}\" alt=\"Logo version VOSTFR pour episode {{ episode.title }} de Dexter\" class=\"logo-version\">
                                </a>
                            </div>
                        </nav>
                        <div class=\"tab-content py-3 px-3 px-sm-0\" id=\"nav-tabContent\">
                            <!--<script src=\"//pubdirecte.com/script/banniere.php?said=128229\"></script>
                            <button class=\"btn btn-secondary btn-show-episode\" id=\"close_pubdirecte\">FERMER PUB</button>-->
                            <div class=\"tab-pane fade show active\" id=\"nav-mixdropvf\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvf-tab\">
                                <div class=\"tab-content\">
                                    {% if episode.OpenloadLinkVf|length > 24 %}
                                        <iframe src=\"{{ episode.OpenloadLinkVf }}\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    {% endif %}
                                    {% if episode.OpenloadLinkVf|length < 24 %}
                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    {% endif %}
                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavf\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavf-tab\">
                                <div class=\"tab-content\">
                                    {% if episode.StreamangoLinkVf|length > 24 %}
                                        <iframe src=\"{{ episode.StreamangoLinkVf }}\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    {% endif %}
                                    {% if episode.StreamangoLinkVf|length < 24 %}
                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    {% endif %}
                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-mixdropvostfr\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvostfr-tab\">
                                <div class=\"tab-content\">
                                    {% if episode.OpenloadLinkVostfr|length > 24 %}
                                        <iframe src=\"{{ episode.OpenloadLinkVostfr }}\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    {% endif %}
                                    {% if episode.OpenloadLinkVostfr|length < 24 %}
                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    {% endif %}
                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavostfr\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavostfr-tab\">
                                <div class=\"tab-content\">
                                    {% if episode.StreamangoLinkVostfr|length > 24 %}
                                        <iframe src=\"{{ episode.StreamangoLinkVostfr }}\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    {% endif %}
                                    {% if episode.StreamangoLinkVostfr|length < 24 %}
                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    {% endif %}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"row mb-5\">
                    <div class=\"col-6 div-btn-show-episode\">
                        {% if episode.saison.id == 1 and episode.number == 1 %}
                            <a href=\"#\" class=\"btn btn-secondary float-left btn-show-episode disabled\">< Episode précédent</a>
                        {% endif %}
                        {% if episode.saison.id >= 1 and episode.number > 1 %}
                        <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id - 1, number: episode.number - 1}) }}\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        {% endif %}
                        {% if episode.saison.id > 1 and episode.number == 1 and episode.saison.id != 8 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id - 1, slug: episode.slug, id: episode.id - 1, number: episode.number + 9}) }}\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        {% endif %}
                        {% if episode.saison.id == 8 and episode.number == 1 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id - 1, slug: episode.slug, id: episode.id - 1, number: episode.number + 6}) }}\" class=\"btn btn-secondary float-left btn-show-episode\" title=\"Regarder episode précédent en streaming gratuit en VF et VOSTFR\">< Episode précédent</a>
                        {% endif %}
                    </div>
                    <div class=\"col-6 div-btn-show-episode\">
                        {% if episode.saison.id == 8 and episode.number == 6 %}
                            <a href=\"#\" class=\"btn btn-secondary float-right btn-show-episode disabled\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id >= 1 and episode.number == 10 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id + 1, slug: episode.slug, id: episode.id + 1, number: episode.number - 9}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id >= 1 and episode.number < 10 and episode.saison.id != 7 and episode.saison.id != 8 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id + 1, number: episode.number + 1}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id == 7 and episode.number == 7 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id + 1, slug: episode.slug, id: episode.id + 1, number: episode.number - 6}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id == 7 and episode.number < 7 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id + 1, number: episode.number + 1}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                        {% if episode.saison.id == 8 and episode.number < 6 %}
                            <a href=\"{{ path('episode.show', {slug_s: episode.saison.slug, id_s: episode.saison.id, slug: episode.slug, id: episode.id + 1, number: episode.number + 1}) }}\" class=\"btn btn-secondary float-right btn-show-episode\" title=\"Regarder episode suivant en streaming gratuit en VF et VOSTFR\">Episode suivant ></a>
                        {% endif %}
                    </div>
                </div>
            </div>
        </section>
    </div>

{% endblock %}
", "episodes/show.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\4streaming-dexter\\templates\\episodes\\show.html.twig");
    }
}
