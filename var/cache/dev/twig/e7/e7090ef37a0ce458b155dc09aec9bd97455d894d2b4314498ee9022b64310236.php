<?php

/* base.html.twig */
class __TwigTemplate_162364e523d53dc5e3e7e94379255fa63e015450f0a986093cbe7d44aa1c01dc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!--<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-136794511-3\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-136794511-3');
        </script> -->

        <meta charset=\"UTF-8\">
        <title>";
        // line 15
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 16
        $this->displayBlock('meta', $context, $blocks);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/app.css"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logo.png"), "html", null, true);
        echo "\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        ";
        // line 21
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>
    <body>
    <nav class=\"navbar fixed-top navbar-expand-lg navbar-dark bg-dark got-nav\">
        <a class=\"navbar-brand link-home\" href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\" title=\"Regarder Dexter en Streaming gratuit VF et VOSTFR\">Dexter Streaming</a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
            <ul class=\"navbar-nav\">
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 32
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 32, $this->source); })()) == "Accueil"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder les saisons de Dexter en streaming VF et VOSTFR gratuit\" href=\"/\">Accueil</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 35
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 35, $this->source); })()) == "Saison 1"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 1 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-1\">Saison 1</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 38
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 38, $this->source); })()) == "Saison 2"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 2 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-2\">Saison 2</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 41
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 41, $this->source); })()) == "Saison 3"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 3 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-3\">Saison 3</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 44
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 44, $this->source); })()) == "Saison 4"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 4 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-4\">Saison 4</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 47
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 47, $this->source); })()) == "Saison 5"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 5 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-5\">Saison 5</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 50
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 50, $this->source); })()) == "Saison 6"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 6 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-6\">Saison 6</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 53
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 53, $this->source); })()) == "Saison 7"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 7 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-7\">Saison 7</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 56
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 56, $this->source); })()) == "Saison 8"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 8 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-8\">Saison 8</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 59
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 59, $this->source); })()) == "Saison 9"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 9 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-9\">Saison 9</a>
                </li>
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link float-left ";
        // line 62
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && ((isset($context["current_menu"]) || array_key_exists("current_menu", $context) ? $context["current_menu"] : (function () { throw new Twig_Error_Runtime('Variable "current_menu" does not exist.', 62, $this->source); })()) == "Autres series"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder d'autres séries\" href=\"/series\">AUTRES SERIES</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <!-- <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d48739dd921c4cf\"></script> -->

        ";
        // line 70
        $this->displayBlock('body', $context, $blocks);
        // line 71
        echo "     <footer class=\"my_footer\">
        <div class=\"container\">
            <h4 class=\"footer-title\">dexter-streaming.com VF ET VOSTFR gratuit</h4>
            <p>
                Ce site n'héberge aucun fichier vidéo. Nous ne faisons que répertorier du contenu se situant sur divers
                hébergeurs légalement reconnus... Si vous constatez quoi que ce soit, veuillez prendre contact avec l'hébergeur en question.
            </p>
        </div>
    </footer>
        ";
        // line 80
        $this->displayBlock('javascripts', $context, $blocks);
        // line 81
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/runtime.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/js/app.js"), "html", null, true);
        echo "\"></script>

    </body>


</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 15
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_meta($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        echo "Regarder toutes les saisons et tous les épisodes de la série Dexter en streaming gratuit!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 21
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 70
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 80
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 80,  268 => 70,  251 => 21,  233 => 16,  215 => 15,  198 => 82,  193 => 81,  191 => 80,  180 => 71,  178 => 70,  165 => 62,  157 => 59,  149 => 56,  141 => 53,  133 => 50,  125 => 47,  117 => 44,  109 => 41,  101 => 38,  93 => 35,  85 => 32,  75 => 25,  70 => 22,  68 => 21,  62 => 18,  58 => 17,  54 => 16,  50 => 15,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!--<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-136794511-3\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-136794511-3');
        </script> -->

        <meta charset=\"UTF-8\">
        <title>{% block title %}Welcome!{% endblock %}</title>
        <meta name=\"description\" content=\"{% block meta %}Regarder toutes les saisons et tous les épisodes de la série Dexter en streaming gratuit!{% endblock %}\">
        <link rel=\"stylesheet\" href=\"{{ asset('build/css/app.css') }}\">
        <link rel=\"icon\" type=\"image/png\" href=\"{{ asset('uploads/logo.png') }}\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        {% block stylesheets %}{% endblock %}
    </head>
    <body>
    <nav class=\"navbar fixed-top navbar-expand-lg navbar-dark bg-dark got-nav\">
        <a class=\"navbar-brand link-home\" href=\"{{ path('home') }}\" title=\"Regarder Dexter en Streaming gratuit VF et VOSTFR\">Dexter Streaming</a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
            <ul class=\"navbar-nav\">
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Accueil\" %} active {% endif %}\" title=\"Regarder les saisons de Dexter en streaming VF et VOSTFR gratuit\" href=\"/\">Accueil</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 1\" %} active {% endif %}\" title=\"Regarder la saison 1 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-1\">Saison 1</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 2\" %} active {% endif %}\" title=\"Regarder la saison 2 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-2\">Saison 2</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 3\" %} active {% endif %}\" title=\"Regarder la saison 3 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-3\">Saison 3</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 4\" %} active {% endif %}\" title=\"Regarder la saison 4 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-4\">Saison 4</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 5\" %} active {% endif %}\" title=\"Regarder la saison 5 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-5\">Saison 5</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 6\" %} active {% endif %}\" title=\"Regarder la saison 6 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-6\">Saison 6</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 7\" %} active {% endif %}\" title=\"Regarder la saison 7 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-7\">Saison 7</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 8\" %} active {% endif %}\" title=\"Regarder la saison 8 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-8\">Saison 8</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Saison 9\" %} active {% endif %}\" title=\"Regarder la saison 9 de Dexter en streaming VF et VOSTFR gratuit\" href=\"/dexter-streaming-saison-9\">Saison 9</a>
                </li>
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link float-left {% if current_menu is defined and current_menu == \"Autres series\" %} active {% endif %}\" title=\"Regarder d'autres séries\" href=\"/series\">AUTRES SERIES</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <!-- <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d48739dd921c4cf\"></script> -->

        {% block body %}{% endblock %}
     <footer class=\"my_footer\">
        <div class=\"container\">
            <h4 class=\"footer-title\">dexter-streaming.com VF ET VOSTFR gratuit</h4>
            <p>
                Ce site n'héberge aucun fichier vidéo. Nous ne faisons que répertorier du contenu se situant sur divers
                hébergeurs légalement reconnus... Si vous constatez quoi que ce soit, veuillez prendre contact avec l'hébergeur en question.
            </p>
        </div>
    </footer>
        {% block javascripts %}{% endblock %}
        <script src=\"{{ asset('build/runtime.js')}}\"></script>
        <script src=\"{{ asset('build/js/app.js')}}\"></script>

    </body>


</html>
", "base.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\4streaming-dexter\\templates\\base.html.twig");
    }
}
