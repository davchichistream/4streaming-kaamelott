<?php

/* admin/episodes/_form.html.twig */
class __TwigTemplate_db7513ce01e2834233bc27a6879166419b7a671b3d9919b4f891931fd4e76ca4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/episodes/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/episodes/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
<div class=\"row\">
    <div class=\"col-md-2\">
        ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 4, $this->source); })()), "saison", []), 'row');
        echo "
    </div>
    <div class=\"col-md-1\">
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->source); })()), "number", []), 'row');
        echo "
    </div>
    <div class=\"col-md-2\">
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->source); })()), "title", []), 'row');
        echo "
    </div>
    <div class=\"col-md-5\">
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->source); })()), "avatar", []), 'row');
        echo "
    </div>
    <div class=\"col-md-2\">
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 16, $this->source); })()), "releasedAt", []), 'row');
        echo "
    </div>
</div>
<h5>VF</h5>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 22, $this->source); })()), "openload_link_vf", []), 'label', ["label" => "Doodstream VF"]);
        echo "
        ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 23, $this->source); })()), "openload_link_vf", []), 'widget');
        echo "
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 29, $this->source); })()), "streamango_link_vf", []), 'label', ["label" => "Vidoza VF"]);
        echo "
        ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->source); })()), "streamango_link_vf", []), 'widget');
        echo "
        <br>
    </div>
</div>
<h5>VOSTFR</h5>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 37, $this->source); })()), "openload_link_vostfr", []), 'label', ["label" => "Doodstream VOSTFR"]);
        echo "
        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 38, $this->source); })()), "openload_link_vostfr", []), 'widget');
        echo "
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 44, $this->source); })()), "streamango_link_vostfr", []), 'label', ["label" => "Vidoza VOSTFR"]);
        echo "
        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 45, $this->source); })()), "streamango_link_vostfr", []), 'widget');
        echo "
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 51, $this->source); })()), "description", []), 'row');
        echo "
    </div>
</div>
<button class=\"btn btn-primary mb-4\">";
        // line 54
        echo twig_escape_filter($this->env, (((isset($context["button"]) || array_key_exists("button", $context))) ? (_twig_default_filter((isset($context["button"]) || array_key_exists("button", $context) ? $context["button"] : (function () { throw new Twig_Error_Runtime('Variable "button" does not exist.', 54, $this->source); })()), "Ajouter")) : ("Ajouter")), "html", null, true);
        echo "</button>
";
        // line 55
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 55, $this->source); })()), 'form_end');
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/episodes/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 55,  127 => 54,  121 => 51,  112 => 45,  108 => 44,  99 => 38,  95 => 37,  85 => 30,  81 => 29,  72 => 23,  68 => 22,  59 => 16,  53 => 13,  47 => 10,  41 => 7,  35 => 4,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ form_start(form) }}
<div class=\"row\">
    <div class=\"col-md-2\">
        {{ form_row(form.saison) }}
    </div>
    <div class=\"col-md-1\">
        {{ form_row(form.number) }}
    </div>
    <div class=\"col-md-2\">
        {{ form_row(form.title) }}
    </div>
    <div class=\"col-md-5\">
        {{ form_row(form.avatar) }}
    </div>
    <div class=\"col-md-2\">
        {{ form_row(form.releasedAt) }}
    </div>
</div>
<h5>VF</h5>
<div class=\"row\">
    <div class=\"col-md-12\">
        {{ form_label(form.openload_link_vf, \"Doodstream VF\") }}
        {{ form_widget(form.openload_link_vf) }}
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        {{ form_label(form.streamango_link_vf, \"Vidoza VF\") }}
        {{ form_widget(form.streamango_link_vf) }}
        <br>
    </div>
</div>
<h5>VOSTFR</h5>
<div class=\"row\">
    <div class=\"col-md-12\">
        {{ form_label(form.openload_link_vostfr, \"Doodstream VOSTFR\") }}
        {{ form_widget(form.openload_link_vostfr) }}
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        {{ form_label(form.streamango_link_vostfr, \"Vidoza VOSTFR\") }}
        {{ form_widget(form.streamango_link_vostfr) }}
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        {{ form_row(form.description) }}
    </div>
</div>
<button class=\"btn btn-primary mb-4\">{{ button|default('Ajouter') }}</button>
{{ form_end(form) }}", "admin/episodes/_form.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\4streaming-dexter\\templates\\admin\\episodes\\_form.html.twig");
    }
}
