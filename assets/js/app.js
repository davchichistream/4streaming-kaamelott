/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you require will output into a single css file (app.scss in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
 const $ = require('jquery');

 require('bootstrap');
 require('jquery-countdown');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();

    $('.custom-file-input').on('change',function(){
        //get the file name
        //$test = document.getElementsByClassName("anchors")[0].id;
        $id = document.getElementsByClassName("custom-file-input")[0].id;
        if($id == "episode_avatar")
        {
            var fileName = document.getElementById('episode_avatar').files[0].name;
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        }
        if($id == "saison_avatar")
        {
            var fileName = document.getElementById('saison_avatar').files[0].name;
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        }
    });

    $("#close_pubdirecte").click(function () {
        $("#pubdirecte").remove();
        $("#close_pubdirecte").remove();
    });

    if ($("#pubdirecte").length == 0) {
        $("#close_pubdirecte").remove();
    }
});




