<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EpisodeRepository")
 */
class Episode
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $openload_link_vf;


    /**
     * @ORM\Column(type="string")
     */
    private $streamango_link_vf;

    /**
     * @ORM\Column(type="string")
     */
    private $openload_link_vostfr;

    /**
     * @ORM\Column(type="string")
     */
    private $streamango_link_vostfr;

    /**
     * @ORM\Column(type="string")
     */
    private $avatar;


    /**
     * @ORM\Column(type="string")
     */
    private $releasedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Saison", inversedBy="episodes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify("episode-");
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getOpenloadLink()
    {
        return $this->openload_link;
    }

    /**
     * @param mixed $openload_link
     */
    public function setOpenloadLink($openload_link)
    {
        $this->openload_link = $openload_link;
    }

    /**
     * @return mixed
     */
    public function getStreamangoLink()
    {
        return $this->streamango_link;
    }

    /**
     * @param mixed $streamango_link
     */
    public function setStreamangoLink($streamango_link)
    {
        $this->streamango_link = $streamango_link;
    }

    /**
     * @return mixed
     */
    public function getReleasedAt()
    {
        return $this->releasedAt;
    }

    /**
     * @param mixed $releasedAt
     */
    public function setReleasedAt($releasedAt)
    {
        $this->releasedAt = $releasedAt;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOpenloadLinkVf()
    {
        return $this->openload_link_vf;
    }

    /**
     * @param mixed $openload_link_vf
     */
    public function setOpenloadLinkVf($openload_link_vf)
    {
        $this->openload_link_vf = $openload_link_vf;
    }

    /**
     * @return mixed
     */
    public function getStreamangoLinkVf()
    {
        return $this->streamango_link_vf;
    }

    /**
     * @param mixed $streamango_link_vf
     */
    public function setStreamangoLinkVf($streamango_link_vf)
    {
        $this->streamango_link_vf = $streamango_link_vf;
    }

    /**
     * @return mixed
     */
    public function getOpenloadLinkVostfr()
    {
        return $this->openload_link_vostfr;
    }

    /**
     * @param mixed $openload_link_vostfr
     */
    public function setOpenloadLinkVostfr($openload_link_vostfr)
    {
        $this->openload_link_vostfr = $openload_link_vostfr;
    }

    /**
     * @return mixed
     */
    public function getStreamangoLinkVostfr()
    {
        return $this->streamango_link_vostfr;
    }

    /**
     * @param mixed $streamango_link_vostfr
     */
    public function setStreamangoLinkVostfr($streamango_link_vostfr)
    {
        $this->streamango_link_vostfr = $streamango_link_vostfr;
    }


}
