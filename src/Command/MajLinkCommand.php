<?php
/**
 * Created by PhpStorm.
 * User: etien
 * Date: 16/04/2019
 * Time: 17:31
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Episode;
use App\Entity\Saison;
use App\Repository\EpisodeRepository;
use App\Repository\SaisonRepository;
use Curl\Curl;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class MajLinkCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:maj-link';

    /**
     * @var EpisodeRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(EpisodeRepository $repository, ObjectManager $em)
    {

        $this->repository = $repository;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Mise Ã  jour des liens Doodstream')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to update Doodstream links on the website')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'LINKS TO UPDATE :',
            '=======================',
        ]);

        $episodes = $this->repository->findAllEpisodes();
        $linkToUpdate = [];
        $update = true;
        $status = ["Not found or not your file", null];
        //return dump($episodes[0]->streamango_link_vf);
        foreach ($episodes as $episode)
        {
            if (strlen($episode->getOpenloadLinkVf()) > 10 && strlen($episode->getOpenloadLinkVostfr()) > 10)
            {
                if(in_array($this->checkLinkDoodstream($this->getLinkId($episode->getOpenloadLinkVf())), $status))
                {
                    $output->writeln($linkToUpdate[] = $episode->getSaison() . " / Episode " . $episode->getNumber() . " / VF / Doodstream");
                }
                if(in_array($this->checkLinkDoodstream($this->getLinkId($episode->getOpenloadLinkVostfr())), $status))
                {
                    $output->writeln($linkToUpdate[] = $episode->getSaison() . " / Episode " . $episode->getNumber() . " / VOSTFR / Doodstream");
                    dump("=====================================");
                }
            }
            else{
                // indiquer par mail or something qu'un lien est invalide
                $output->writeln('Erreur: le lien ' . $episode->getSaison() . " / Episode " . $episode->getNumber() . ' est invalide ! ');
                dump("=====================================");
            }
        }
        // fin du script return pas de maj effectuÃ©e tout est ok les logs.
        if (count($linkToUpdate) === 0)
        {
            $update = false;
            $output->writeln([
                '=======================',
                'Tous les liens fontionnent, pas de maj necessaire !',
            ]);
        }
        if ($update == true && $linkToUpdate > 0)
        {
            //return dump($linkToUpdate);
        }
    }


    /**
     * @param $link
     * @return mixed
     */
    public function getLinkId($link)
    {
        $array = explode("/", $link);
        return $array[4];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function checkLinkDoodstream($id)
    {
        $curl = new Curl();
        $status = "";
        $fileInfo = $curl->get('https://doodapi.com/api/file/info',[
            'key' => '6449trbpjc3kcir0rl0j',
            'file_code' => $id,
        ]);
        $file = json_decode($fileInfo->getResponse(), true);
        if(isset($file["result"]))
        {
            $status = $file["result"][0]["status"];
        }
        return $status;
    }
}