<?php
/**
 * Created by PhpStorm.
 * User: etien
 * Date: 17/02/2019
 * Time: 21:06
 */

namespace App\Controller;


use App\Repository\SaisonRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Saison;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class SaisonsController extends AbstractController
{

    /**
     * @var SaisonRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(SaisonRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index()
    {

        $repository = $this->getDoctrine()->getRepository(Saison::class);
        //dump($repository);
        $saisons = $repository->findAll();
        //dump($saison);
        return $this->render('saisons/index.html.twig', [
           'saisons' => $saisons,
            'current_menu' => 'Accueil'
        ]);
    }

    /**
     * @Route("/series", name="series")
     * @return Response
     */
    public function series()
    {
        return $this->render('saisons/series.html.twig', [
            'current_menu' => 'Autres series'
        ]);
    }

    /**
     * @Route("/{slug}-{id}", name="saison.show", requirements={"slug": "[a-z0-9\-]*"})
     * @param Saison $saison
     * @param $slug
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function show(Saison $saison, $slug, Breadcrumbs $breadcrumbs)
    {
        $breadcrumbs->addItem("Home", $this->get("router")->generate("home"));

        $breadcrumbs->addItem(" " . $saison->getName(), $this->get("router")->generate("saison.show", [
            'id' => $saison->getId(),
            'slug' => $saison->getSlug()
        ]));

        $episodes = $saison->getEpisodes();
        if($saison->getSlug() !== $slug)
        {
            return $this->redirectToRoute("saison.show", [
                'id' => $saison->getId(),
                'slug' => $saison->getSlug()
            ], 301);
        }
            return $this->render('saisons/show.html.twig', [
                'saison' => $saison,
                'current_menu' => $saison->getName(),
                'episodes' => $episodes
            ]);
    }
}