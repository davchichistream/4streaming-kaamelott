<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221001942 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE episode CHANGE openload_link_vf openload_link_vf VARCHAR(255) NOT NULL, CHANGE upvid_link_vf upvid_link_vf VARCHAR(255) NOT NULL, CHANGE openload_link_vostfr openload_link_vostfr VARCHAR(255) NOT NULL, CHANGE upvid_link_vostfr upvid_link_vostfr VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE episode CHANGE openload_link_vf openload_link_vf LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE upvid_link_vf upvid_link_vf LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE openload_link_vostfr openload_link_vostfr LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE upvid_link_vostfr upvid_link_vostfr LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
